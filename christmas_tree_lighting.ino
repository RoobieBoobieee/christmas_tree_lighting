#include <FastLED.h>

#define LED_PIN     2
#define NUM_LEDS    5
#define TOGGLE_PIN  3
#define UPDATES_PER_SECOND 10

CRGB leds[NUM_LEDS];
CRGBPalette16 currentPalette;

unsigned long last_interrupt_time = 0;
uint8_t startIndex = 0;
int mode = 0;

void setup() {
  delay(2000);

  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);

  fill_solid(currentPalette, 16, CRGB::White);
  
  pinMode(TOGGLE_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(TOGGLE_PIN), next, RISING);
}

void loop() {
  startIndex = startIndex + 1;

  FillLEDsFromPaletteColors(startIndex);

  FastLED.show();
  FastLED.delay(1000 / UPDATES_PER_SECOND);
}


void next() {
  if (millis() - last_interrupt_time > 200)
  {
    mode++;
    if (mode > 6) {
      mode = 0;
    }

    switch (mode) {
      case 0:
        fill_solid(currentPalette, 16, CRGB::White);
        break;
      case 1:
        fill_solid(currentPalette, 16, CRGB::Red);
        break;
      case 2:
        fill_solid(currentPalette, 16, CRGB::Green);
        break;
      case 3:
        fill_solid(currentPalette, 16, CRGB::Blue);
        break;
      case 4:
        currentPalette = RainbowColors_p;
        break;
      case 5:
        currentPalette = CloudColors_p;
        break;
      case 6:
        setupChristmasPalette();
        break;
    }
    Serial.println(mode);
  }
  last_interrupt_time = millis();
}


void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette(currentPalette, colorIndex, 255, LINEARBLEND);
    colorIndex += 3;
  }
}


void setupChristmasPalette()
{
  CRGB red = CHSV( HUE_RED, 255, 255);
  CRGB green = CHSV( HUE_GREEN, 255, 255);

  currentPalette = CRGBPalette16(red, red, red, red, red, red, red, red, green, green, green, green, green, green, green, green);
}
